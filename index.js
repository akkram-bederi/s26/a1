//1.What directive is used by Node.js in loading the modules it needs?
    //require() 

//2.What Node.js module contains a method for server creation?
    //HTTP Module

//3. What is the method of the http object responsible for creating a server using Node.js?
    //createServer()

//4.What method of the response object allows us to set status codes and content types?
    //res.writeHead

//5.Where will console.log() output its contents when run in Node.js?
    //Local Terminal/Local Machine

//6.What property of the request object contains the address' endpoint?
    //request.url

    const HTTP = require('http');

    HTTP.createServer((request, response) => {
        console.log(`Hi!`);
        if(request.url==="/login"){
            response.writeHead(200,{"Content-Type":"text/plain"});
            response.write("You are in the login page");
            response.end()
        }else
        {
            response.writeHead(400,{"Content-Type":"text/plain"});
            response.write("Error");
            response.end()
        }
    }).listen(4000)